#include "minirys_ros2/helpers/PIDRegulator.hpp"

#include <algorithm>

PIDRegulator::PIDRegulator():
	looptime(0.0),
	kp(0),
	ki(0),
	kd(0),
	max(0),
	setpoint(0.0),
	prevError(0.0),
	integral(0.0) {}

void PIDRegulator::setParams(std::chrono::duration<double> looptime, double kp, double ki, double kd, double max) {
	this->kp = kp;
	this->ki = ki;
	this->kd = kd;
	this->max = max;
	this->looptime = looptime.count();
	this->zero();
}

double PIDRegulator::update(double setpoint, double value) {
	// It's just a regular everyday normal pidregulator
	double error = setpoint - value;
	this->integral += this->ki * error * looptime;
	// Clamp integral value (simple anti-windup)
	this->integral = std::min(std::max(this->integral, -this->max), this->max);
	double derivative = this->kd * (error - this->prevError) / looptime;
	double output = this->kp * error + this->integral + derivative;
	this->prevError = error;

	return std::min(std::max(output, -this->max), this->max);
}

void PIDRegulator::zero() {
	this->setpoint = 0.0;
	this->prevError = 0.0;
	this->integral = 0.0;
}
