#pragma once

#include <chrono>

class PIDRegulator {
public:
	PIDRegulator();

	~PIDRegulator() = default;

	void setParams(std::chrono::duration<double> looptime, double kp, double ki, double kd, double max);

	double update(double setpoint, double value);

	void zero();

private:
	double looptime;
	double kp;
	double ki;
	double kd;
	double max;

	double setpoint;
	double prevError;
	double integral;
};
